#include "../cpu/isr.h"
#include "../drivers/screen.h"
#include "kernel.h"
#include "../libc/string.h"
#include "../libc/mem.h"
#include <stdint.h>

void kernel_main() {
    isr_install();
    irq_install();

    asm("int $2");
    asm("int $3");

    kprint("Type HELP for more info!\n> ");
}

void user_input(char *input) {
    if (strcmp(input, "HALT") == 0) {
        kprint("Halting the System. Bye!\n");
        asm volatile("hlt");
    } else if (strcmp(input, "PAGE") == 0) {
        uint32_t phys_addr;
        uint32_t page = kmalloc(1000, 1, &phys_addr);
        char page_str[16] = "";
        hex_to_ascii(page, page_str);
        char phys_str[16] = "";
        hex_to_ascii(phys_addr, phys_str);
        kprint("Page: ");
        kprint(page_str);
        kprint(", physical address: ");
        kprint(phys_str);
    } else if (strcmp(input, "HELP") == 0) {
        kprint(
        " ---------- HELP -----------\n"
        "|  Help - Show this page    |\n"
        "| About - Info about Kernel |\n"
        "| Page - requests a kmalloc |\n"
        "| Halt - halt cpu operation |\n"
        " ---------------------------"
        );
    } else if (strcmp(input, "ABOUT") == 0) {
        kprint(
        " -------- ABOUT ---------\n"
        "|  SYNAPSE KERNEL 0.0.2  |\n"
        "|     (c) 2020 Riku      |\n"
        " ------------------------"
    );
    } else {
        kprint("You said: ");
        kprint(input);
    }
    kprint("\n> ");
}
